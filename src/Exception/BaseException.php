<?php
/**
 * Created by PhpStorm.
 * User: terrywang
 * Date: 24/06/2017
 * Time: 3:25 PM
 */

namespace Galaxy\Helpers\Exception;


class BaseException extends \Exception
{

    protected $detail;

    public function __construct($message = "", $code = 0, \Exception $previous = null, $detail = null)
    {
        parent::__construct($message, $code, $previous);
        $this->detail = $detail;
    }

    public function getDetail()
    {
        return $this->detail;
    }

    public function setDetail($detail)
    {
        $this->detail = $detail;
    }

    public static function fromErrorResponse(array $errorObject)
    {
        $class = $errorObject['exception'] ?: self::class;


        return new $class(
            array_get($errorObject, 'message'),
            array_get($errorObject, 'code'),
            null,
            array_get($errorObject, 'detail'));
    }

    public static function fromException(\Exception $e, $detail = null)
    {
        $class = self::class;
        return new $class($e->getMessage(), $e->getCode(), $e, $detail?? $e->getTrace());

    }

    public function toErrorResponse()
    {
        return [
            'statusCode' => (int)($this->getCode() / 100),
            'code' => $this->getCode(),
            'message' => $this->getMessage(),
            'detail' => $this->detail
        ];
    }

}
