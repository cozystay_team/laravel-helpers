<?php
/**
 * Created by PhpStorm.
 * User: terrywang
 * Date: 19/07/2017
 * Time: 6:01 PM
 */

namespace Galaxy\Helpers\EndPoint;


use ReflectionClass;

class PermissionGenerator
{

    public static function generatePermissionDataList(string $className)
    {
        $oClass = new ReflectionClass($className);
        $endPoints = $oClass->getConstants();

        $permissionList = [];
        array_walk($endPoints, function ($endPoint, $name) use (&$permissionList) {

            if (isset($endPoint['needPermission']) && $endPoint['needPermission']) {
                $name = str_replace("_", ".", strtolower($name));
                $display_name = str_replace(".", " ", $name);
                $description = $display_name;
                $permissionList[] = compact('description', 'display_name', 'name');
            }
        });
        return $permissionList;
    }

    public static function generateEndPointList(string $className)
    {

        $oClass = new ReflectionClass($className);
        $endPoints = $oClass->getConstants();

        array_walk($endPoints, function (&$endPoint, $name) {
            if (isset($endPoint['needPermission']) && $endPoint['needPermission']) {
                $name = str_replace("_", ".", strtolower($name));
                if (!isset($endPoint['middleware'])) {
                    $endPoint['middleware'] = "glxy.checkpermission:$name";
                } else if (is_array($endPoint['middleware'])) {
                    $endPoint['middleware'] = array_merge($endPoint['middleware'], ["glxy.checkpermission:$name"]);
                } else {
                    $endPoint['middleware'] = [$endPoint['middleware'], "glxy.checkpermission:$name"];
                }
            }
        });

        return $endPoints;
    }
}