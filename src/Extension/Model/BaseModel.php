<?php
/**
 * Created by PhpStorm.
 * User: terrywang
 * Date: 2017/5/10
 * Time: 17:32
 */

namespace Galaxy\Helpers\Extension\Model;


use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model implements EncodableAndTranslatableInterface
{
    use  BootTrait, ValidateTrait;

    public $incrementing = false;
    public $allLanguagesTranslations;
    public $translationFields = [];
    protected $encodeDecodeFields = [];
    protected $rules;
    protected $decodes;
    protected $enumeration = [];
    private $processed = false;
//    protected $hidden = ['key', 'value', 'related_id', 'namespace','lang']; // hide attributes of translations table


    public function processed(): bool
    {
        return $this->processed;
    }

    public function setProcessed(bool $processed)
    {
        $this->processed = $processed;
    }

    public function getDirty()
    {
        $dirties = parent::getDirty();
        $filteredDirty = [];
        foreach ($dirties as $key => $dirty) {
            if (!in_array($key, $this->encodeDecodeFields)) {
                $filteredDirty[$key] = $dirty;
                continue;
            }
            $originalVal = $this->getOriginal($key);
            if (!is_string($originalVal) || !is_array($dirty)) {
                $filteredDirty[$key] = $dirty;
                continue;
            }
            $dirtyStr = json_encode($dirty);
            $originalStr = json_encode(json_decode($originalVal, true));
            if (strcmp($dirtyStr, $originalStr) != 0) {
                $filteredDirty[$key] = $dirty;
                continue;
            }
        }
        return $filteredDirty;
    }

    public function beforeSave()
    {
    }

    public function getTranslationFields()
    {
        return $this->translationFields;
    }

    public function attachEnumerations($enumQuery)
    {
        $fields = explode(',', $enumQuery);
        $result = [];
        foreach ($fields as $field) {
            if($model = array_get($this->enumeration, $field)) {
                $result[$field] = $model::get();
            }
        }
        $this->enum = $result;
    }
}