<?php
/**
 * Created by PhpStorm.
 * User: terrywang
 * Date: 27/06/2017
 * Time: 10:56 AM
 */

namespace Galaxy\Helpers\Exception;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use PDOException;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Request;


class ExceptionMapping
{

    const DEFAULT_EXCEPTION_CODE = 50000;


    public static $responseCode = [
        40000 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Bad Request',
            'message_zh-cn' => '无效请求',
            'message_zh-tw' => '無效請求'
        ],
        40001 => [
            'exception' => GLValidationException::class,
            'message_en' => 'Validation Failed',
            'message_zh-cn' => '验证失败',
            'message_zh-tw' => '驗證失敗'
        ],
        40002 => [
            'exception' => GLSystemException::class,
            'message_en' => 'End Date Is Larger Than Start Date',
            'message_zh-cn' => '开始日期大于截止日期',
            'message_zh-tw' => '開始日期大於截止日期'
        ],
        40003 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Parameters Contain Non-fillable Attributes: %s ',
            'message_zh-cn' => '参数包含不可填写的属性：%s',
            'message_zh-tw' => '參數包含不可填寫的屬性：%s'
        ],
        40004 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Parameters Contain Illegal Field',
            'message_zh-cn' => '参数中包含敏感字段',
            'message_zh-tw' => '參數中包含敏感字段'
        ],
        40005 => [
            'exception' => GLSystemException::class,
            'message_en' => 'User Id Required',
            'message_zh-cn' => '缺少用户ID',
            'message_zh-tw' => '缺少用戶ID'
        ],
        40006 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Order does not belong to current user',
            'message_zh-cn' => '该订单不属于当前用户',
            'message_zh-tw' => '該訂單不屬於當前用戶'
        ],
        40007 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Order does not support this action',
            'message_zh-cn' => '该订单不允许当前操作',
            'message_zh-tw' => '該訂單不允許當前操作'
        ],
        40008 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Order status is not allowed',
            'message_zh-cn' => '订单状态错误',
            'message_zh-tw' => '訂單狀態錯誤'
        ],
        40009 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Order status table error',
            'message_zh-cn' => '订单状态表错误',
            'message_zh-tw' => '訂單狀態表錯誤'
        ],

        40010 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Order has been declined',
            'message_zh-cn' => '该订单已被拒绝',
            'message_zh-tw' => '該訂單已被拒絕'
        ],
        40011 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Payment Complete Error',
            'message_zh-cn' => '完成支付错误',
            'message_zh-tw' => '完成支付錯誤'
        ],
        40012 => [
            'exception' => GLSystemException::class,
            'message_en' => 'No language header provided',
            'message_zh-cn' => 'header中缺少语言设定',
            'message_zh-tw' => 'header中缺少語言設定'
        ],
        40013 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Payment Failed',
            'message_zh-cn' => '支付失败',
            'message_zh-tw' => '支付失敗'
        ],
        40014 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Phone number is invalid',
            'message_zh-cn' => '手机号码不正确',
            'message_zh-tw' => '手機號碼不正確'
        ],
        40015 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Listing is not available during selected dates',
            'message_zh-cn' => '该房间在所选日期内不可用',
            'message_zh-tw' => '該房間在所選日期內不可用'
        ],
        40016 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Order already have a voucher',
            'message_zh-cn' => '该订单已经使用了优惠券',
            'message_zh-tw' => '該訂單已經使用了優惠券'
        ],
        40017 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Voucher in your order is invalid',
            'message_zh-cn' => '订单中的优惠券不可用',
            'message_zh-tw' => '訂單中的優惠券不可用'
        ],
        40018 => [
            'exception' => GLSystemException::class,
            'message_en' => 'System language setting is incorrect',
            'message_zh-cn' => '语言设置失败',
            'message_zh-tw' => '語言設置失敗'
        ],
        40019 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Captcha code is invalid',
            'message_zh-cn' => '图形验证码错误',
            'message_zh-tw' => '圖形驗證碼錯誤'
        ],

        40020 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Payment status abnormal',
            'message_zh-cn' => '支付状态错误',
            'message_zh-tw' => '支付狀態錯誤'
        ],
        40021 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Can not have payer and payee info at same time',
            'message_zh-cn' => '不能同时获取付款人和收款人信息',
            'message_zh-tw' => '不能同時獲取付款人和收款人訊息'
        ],
        40022 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Specified payer/payee type but no user is found',
            'message_zh-cn' => '付款人或收款人不存在',
            'message_zh-tw' => '付款人或收款人不存在'
        ],
        40023 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Check out date must be greater than Check in date',
            'message_zh-cn' => '退房日期必须大于入住日期',
            'message_zh-tw' => '退房日期必須大於入住日期'
        ],
        40024 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Check in date must be greater than Advance Notice Day',
            'message_zh-cn' => '预订日期不符合房东设定的提前通知天数',
            'message_zh-tw' => '預訂日期不符合房東設定的提前通知天數'
        ],
        40025 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Booking date is beyond booking window day',
            'message_zh-cn' => '预订日期超出房东设置的预订时间段',
            'message_zh-tw' => '預訂日期超出房東設置的預訂時間段'
        ],
        40026 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Booking days need to be greater than minimum stay days',
            'message_zh-cn' => '预订天数必须大于最少预订天数',
            'message_zh-tw' => '預訂天數必須大於最少預訂天數'
        ],
        40027 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Booking days need to be less than maximum stay days',
            'message_zh-cn' => '预订天数必须少于最多预订天数',
            'message_zh-tw' => '預訂天數必須少於最多預訂天數'
        ],
        40028 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Provide correct phone number and verification code to save your phone number',
            'message_zh-cn' => '请提供完整正确手机号及验证码以便验证您的手机号码',
            'message_zh-tw' => '請提供完整正確手機號及驗證碼以便驗證您的手機號碼'
        ],
        40029 => [
            'exception' => GLSystemException::class,
            'message_en' => 'You can not book your own listing',
            'message_zh-cn' => '不能预订自己的房屋',
            'message_zh-tw' => '不能預訂自己的房屋'
        ],
        40030 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Payment method is incorrect',
            'message_zh-cn' => '所选支付渠道不存在',
            'message_zh-tw' => '所選支付渠道不存在'
        ],
        40031 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Refund amount is incorrect',
            'message_zh-cn' => '退款金额不正确',
            'message_zh-tw' => '退款金额不正确'
        ],
        40032 => [
            'exception' => GLSystemException::class,
            'message_en' => 'You have started payment, please view your order in your orders ',
            'message_zh-cn' => '已开始支付，请到订单列表中查看',
            'message_zh-tw' => '已开始支付，请到订单列表中查看'
        ],
        40033 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Order submitted, update not allowed',
            'message_zh-cn' => '不可修改已提交订单',
            'message_zh-tw' => '不可修改已提交訂單'
        ],
        40034 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Please input valid phone number',
            'message_zh-cn' => '请输入有效的手机号',
            'message_zh-tw' => '请输入有效的手机号'
        ],
        40035 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Order Created, you may not change currency',
            'message_zh-cn' => '订单已生成，不可更改货币',
            'message_zh-tw' => '订单已生成，不可更改货币'
        ],
        40036 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Guest number exceeds max capacity ',
            'message_zh-cn' => '入住人数超过房屋可容纳数量',
            'message_zh-tw' => '入住人数超过房屋可容纳数量'
        ],





        40100 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => 'Action Requires Login',
            'message_zh-cn' => '操作前请先登录',
            'message_zh-tw' => '操作前請先登錄'
        ],
        40101 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => 'Insufficient Permission For Current Operation',
            'message_zh-cn' => '权限不足',
            'message_zh-tw' => '權限不足'
        ],
        40102 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => 'Could Not Find Your Token',
            'message_zh-cn' => '令牌不存在',
            'message_zh-tw' => '令牌不存在'
        ],
        40103 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => 'Token Expired',
            'message_zh-cn' => '令牌已过期',
            'message_zh-tw' => '令牌已過期'
        ],
        40104 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => 'Login Failed',
            'message_zh-cn' => '登录失败',
            'message_zh-tw' => '登錄失敗'
        ],
        40105 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => 'No Social Tokens Found',
            'message_zh-cn' => '第三方登录错误',
            'message_zh-tw' => '第三方登錄錯誤'
        ],
        40106 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => 'Social Tokens Already Registered',
            'message_zh-cn' => '第三方平台账号已存在',
            'message_zh-tw' => '第三方平台賬號已存在'
        ],
        40107 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => 'Operation Not Permitted For Current User',
            'message_zh-cn' => '权限不足',
            'message_zh-tw' => '權限不足'
        ],
        40108 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => 'User is banned.',
            'message_zh-cn' => '用户被禁用',
            'message_zh-tw' => '用戶被禁用'
        ],
        40109 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => 'User Already Registered.',
            'message_zh-cn' => '账号已存在',
            'message_zh-tw' => '賬號已存在'
        ],
        40110 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => 'User not exists.',
            'message_zh-cn' => '用户不存在',
            'message_zh-tw' => '用戶不存在'
        ],
        40111 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => 'User exists but not bind.',
            'message_zh-cn' => '请绑定第三方平台账号',
            'message_zh-tw' => '請綁定第三方平台賬號'
        ],
        40112 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => 'User email does not exist',
            'message_zh-cn' => '邮箱不存在',
            'message_zh-tw' => '郵箱不存在'
        ],
        40113 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => 'Error while reading token',
            'message_zh-cn' => '获取令牌时出错',
            'message_zh-tw' => '獲取令牌時出錯'
        ],
        40114 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => 'Already logged in',
            'message_zh-cn' => '您已经登录了',
            'message_zh-tw' => '您已經登錄了'
        ],
        40115 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => 'Listing Status Locked',
            'message_zh-cn' => '房源状态被锁',
            'message_zh-tw' => '房源狀態被鎖'
        ],
        40116 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => 'Phone number is occupied',
            'message_zh-cn' => '该手机号已存在账户',
            'message_zh-tw' => '该手机号已存在账户'
        ],
        40117 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => 'Email is occupied',
            'message_zh-cn' => '该邮箱已存在账户',
            'message_zh-tw' => '该邮箱已存在账户'
        ],
        40118 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => 'Incorrect Login credential',
            'message_zh-cn' => '登录账户/密码不正确',
            'message_zh-tw' => '登录账户/密码不正确'
        ],
        40119 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => 'Invalid signature',
            'message_zh-cn' => '验证签名失败',
            'message_zh-tw' => '驗證簽名失敗'
        ],

        40200 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Payment Required',
            'message_zh-cn' => '请先完成支付',
            'message_zh-tw' => '請先完成支付'
        ],
        40300 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Forbidden',
            'message_zh-cn' => '禁用',
            'message_zh-tw' => '禁用'
        ],
        40301 => [
            'exception' => GLDataException::class,
            'message_en' => 'User Not Exist In Conversation',
            'message_zh-cn' => '用户不在当前对话中',
            'message_zh-tw' => '用戶不在當前對話中'
        ],
        40302 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Cannot redeem more vouchers of this type',
            'message_zh-cn' => '不能获取更多此类代金券',
            'message_zh-tw' => '不能獲取更多此類代金券'
        ],
        40400 => [
            'exception' => GLSystemException::class,
            'message_en' => "Route Not Found",
            'message_zh-cn' => '路径不存在',
            'message_zh-tw' => '路徑不存在'
        ],
        40401 => [
            'exception' => GLDataException::class,
            'message_en' => 'No Record With Such Id',
            'message_zh-cn' => '记录不存在',
            'message_zh-tw' => '記錄不存在'
        ],
        40402 => [
            'exception' => GLDataException::class,
            'message_en' => 'No Such User',
            'message_zh-cn' => '用户不存在',
            'message_zh-tw' => '用戶不存在'
        ],
        40403 => [
            'exception' => GLDataException::class,
            'message_en' => 'Invalid Referral ID',
            'message_zh-cn' => '推荐人不存在',
            'message_zh-tw' => '推薦人不存在'
        ],
        40404 => [
            'exception' => GLDataException::class,
            'message_en' => 'No Orders satisfy condition',
            'message_zh-cn' => '没有满足查询条件的记录',
            'message_zh-tw' => '沒有符合查詢條件的記錄'
        ],
        40405 => [
            'exception' => GLDataException::class,
            'message_en' => 'No Such Permission For Current Operation',
            'message_zh-cn' => '缺少当前操作对应的权限配置',
            'message_zh-tw' => '缺少當前操作對應的權限配置'
        ],
        40406 => [
            'exception' => GLDataException::class,
            'message_en' => 'Invalid Promo Code',
            'message_zh-cn' => '优惠码错误',
            'message_zh-tw' => '優惠碼錯誤'
        ],
        40410 => [
            'exception' => GLDataException::class,
            'message_en' => 'No such listing, or listing is not listed',
            'message_zh-cn' => '房源不存在，或者房源未公开',
            'message_zh-tw' => '房源不存在，或者房源未公開'
        ],

        40500 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Method Not Allowed',
            'message_zh-cn' => '非法操作',
            'message_zh-tw' => '非法操作'
        ],

        40800 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Request Timeout',
            'message_zh-cn' => '请求超时',
            'message_zh-tw' => '請求超時'
        ],

        40900 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Conflict',
            'message_zh-cn' => '冲突',
            'message_zh-tw' => '衝突'
        ],
        40901 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Duplicate Primary Key Found',
            'message_zh-cn' => '主键重复',
            'message_zh-tw' => '主鍵重複'
        ],
        40902 => [
            'exception' => GLSystemException::class,
            'message_en' => "More Than One Listing Request For A Listing",
            'message_zh-cn' => '重复的房源发布操作',
            'message_zh-tw' => '重複的房源發佈操作'
        ],
        42900 => [
            'exception' => GLSystemException::class,
            'message_en' => "Too Many Attempts",
            'message_zh-cn' => '请求次数过多',
            'message_zh-tw' => '請求次數過多'
        ],
        42901 => [
            'exception' => GLAuthorizeException::class,
            'message_en' => "Too Many Attempts",
            'message_zh-cn' => '错误尝试过多',
            'message_zh-tw' => '錯誤嘗試過多'
        ],

        50000 => [
            'exception' => GLUnknownException::class,
            'message_en' => 'Unknown Exception',
            'message_zh-cn' => '未知错误',
            'message_zh-tw' => '未知錯誤'
        ],

        50001 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Database Exception',
            'message_zh-cn' => '数据库错误',
            'message_zh-tw' => '數據庫錯誤'
        ],

        50002 => [
            'exception' => InterSystemException::class,
            'message_en' => 'Calling Subsystem Failed',
            'message_zh-cn' => '子系统调用错误',
            'message_zh-tw' => '子系統調用錯誤'
        ],
        50003 => [
            'exception' => ObjectParseException::class,
            'message_en' => 'Failed To Parse Subsystem Result',
            'message_zh-cn' => '子系统返回结果无法解析',
            'message_zh-tw' => '子系統返回結果無法解析'
        ],
        50004 => [
            'exception' => GLDataException::class,
            'message_en' => 'Failed To Insert Such Record',
            'message_zh-cn' => '数据插入失败',
            'message_zh-tw' => '數據插入失敗'
        ],
        50005 => [
            'exception' => GLDataException::class,
            'message_en' => 'Failed To Update Such Record',
            'message_zh-cn' => '数据更新失败',
            'message_zh-tw' => '數據更新失敗'
        ],
        50006 => [
            'exception' => GLDataException::class,
            'message_en' => 'Failed To Delete Such Record',
            'message_zh-cn' => '记录删除失败',
            'message_zh-tw' => '記錄刪除失敗'
        ],
        50007 => [
            'exception' => GLDataException::class,
            'message_en' => 'No Record In Cache',
            'message_zh-cn' => '缓存中找不到该记录',
            'message_zh-tw' => '緩存中找不到該條記錄'
        ],
        50008 => [
            'exception' => GLDataException::class,
            'message_en' => 'Search Index Not Initialized',
            'message_zh-cn' => '搜索索引初始化失败',
            'message_zh-tw' => '搜索索引初始化失敗'
        ],
        50009 => [
            'exception' => GLDataException::class,
            'message_en' => 'Order controller config incorrect',
            'message_zh-cn' => '订单配置错误',
            'message_zh-tw' => '訂單配置錯誤'
        ],
        50010 => [
            'exception' => GLDataException::class,
            'message_en' => 'SMS Delivery Failure',
            'message_zh-cn' => '短信发送失败',
            'message_zh-tw' => '簡訊傳送失敗'
        ],
        50011 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Template required data missing',
            'message_zh-cn' => '模板数据缺失',
            'message_zh-tw' => '模板数据缺失'
        ],
        50012 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Number of Records mis-match',
            'message_zh-cn' => '您提供的数据数量与库裡的对不上',
            'message_zh-tw' => '您提供的數據數量與庫裡的對不上'
        ],
        50013 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Missing data - sequence',
            'message_zh-cn' => '您提供的数据缺少 sequence 值',
            'message_zh-tw' => '您提供的数据缺少 sequence 值'
        ],
        50014 => [
            'exception' => GLSystemException::class,
            'message_en' => 'Duplicate sequence value',
            'message_zh-cn' => '您提供的数据 sequence 值重複了',
            'message_zh-tw' => '您提供的数据 sequence 值重複了'
        ]

    ];

    private static function getMappings()
    {
        return [
            NotFoundHttpException::class => 40400,
            MethodNotAllowedHttpException::class => 40500,
            MassAssignmentException::class => ['code' => 40003, 'customize_message' => true],
            ModelNotFoundException::class=>40401,
            FatalThrowableError::class => 50000,
            PDOException::class => 50001,
            ValidationException::class => 40001
        ];

    }

    public static function exceptionFromErrorCode(int $code, $detail = null)
    {
        $response = self::_filterMessage(self::$responseCode[$code] ?: self::$responseCode[self::DEFAULT_EXCEPTION_CODE]);

        $fields = compact('code', 'detail');

        $fields = array_filter($fields);


        $response = array_merge($response, $fields);


        return BaseException::fromErrorResponse($response);

    }

    public static function convertToResponse(\Exception $e)
    {
        $class = get_class($e);

        $message = $e->getMessage();

        if (is_subclass_of($e, BaseException::class) && $e->getDetail()) {
            $detail = $e->getDetail();
        } else if ($e instanceof ValidationException) {
            $detail = $e->validator->getMessageBag()->getMessages();
        } else {
            $detail = $e->getTraceAsString();
        }
        if ($codeValue = array_get(self::getMappings(), $class)) {
            if (is_array($codeValue)) {
                $code = $codeValue['code'];
            } else {
                $code = $codeValue;
            }
            $response = self::$responseCode[$code];
            if (is_array($codeValue)
                && $codeValue['customize_message']
                && $response['message']
            ) {
                $message = sprintf($response['message'], $message);
            }

            //looking for matches in mappings

        } else if ($response = array_get(self::$responseCode, $e->getCode())) {
            $code = $e->getCode();
            //looking for matches in codes
        } else {
            $response = self::$responseCode[self::DEFAULT_EXCEPTION_CODE];
            $code = self::DEFAULT_EXCEPTION_CODE;
            //fall back to default error code

        }

        $status_code = array_get($response, 'status_code', (int)($code / 100));

        $fields = compact('code', 'detail', 'status_code', 'message');

        $fields = array_filter($fields);

        return array_merge(
            $fields,
            array_except(self::_filterMessage($response), 'exception')
        );


    }

    private static function _filterMessage($response) {
        $headerLang = Request::header('coz-lang');
        $headerLang = strtolower($headerLang ?: Cookie::get('coz-lang'));
        $lang = in_array($headerLang, ['en', 'zh-cn', 'zh-tw'], true) ? $headerLang : 'en';
        $message = $response["message_$lang"];
        $messageKeys = [];
        foreach (array_keys($response) as $k) {
            if(strpos($k, 'message') === 0) {
                $messageKeys[] = $k;
            }
        }
        return array_merge(array_except($response, $messageKeys), compact('message'));
    }


}