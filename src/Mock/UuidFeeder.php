<?php
/**
 * Created by PhpStorm.
 * User: qiyuzhao
 * Date: 2017-06-22
 * Time: 4:50 PM
 */

namespace Galaxy\Helpers\Mock;


trait UuidFeeder
{
    public static $uuids = [
        'b631d230-4d6a-4146-b6db-a96be3c2b246',
        '3d43b409-f7e5-47e7-879e-e24bb140430a',
        'bb639ad9-0dcb-43d5-96a9-cc87bec34437',
        'ecb32650-630e-4277-a9d7-bb4339d3d0b8',
        'b935fef9-35a3-4aed-bf8a-35f7b9a2d7e2',
        '97930777-860a-4dde-86ee-2bb6336afa90',
        'cb763f54-3eef-4755-9fe1-7540f7b67e1d',
        '49ee9e10-4907-4ced-80c1-966da4b7daf6',
    ];
    public static $testUuids = [
        '062dbc02-2bc9-458d-b57e-b7ff10f98159',
        '0315b714-eea9-4e4f-b667-db6b9cbade66',
        'ea76bde3-aa15-47c5-a84f-533f58a24a26',
        '9f49683b-b556-4836-9090-8c7ceee2185c',
        '5a1293ca-f34a-4f19-978b-cbab0d5c20d1',
    ];

    public static $conversationIds = [
        'a41b5b00-71c0-11e7-8542-414fd3b932e2',
        'a41bdfa0-71c0-11e7-b34e-991eb0948f08',
        'a41c4e50-71c0-11e7-9d5b-b5c84e2b8ac3',
        'a41cbfe0-71c0-11e7-bd94-a92a709453dd'
    ];

    public static $messageTemplateIds = [
        '27be38d0-7223-11e7-9efa-89da4f763add',
        '27c2a9e0-7223-11e7-b257-77885d1f1b96'
    ];

    public function Uuid()
    {
        return \Faker\Provider\Uuid::uuid();
    }

    private $baseUuid = '27c2a9e0-7223-11e7-b257-';
    private $baseNumber = 0;

    public function linearNextUuid() {
        // eg: 27c2a9e0-7223-11e7-b257-000000000001
        return $this->baseUuid .  sprintf('%012d', $this->baseNumber++);
    }
}