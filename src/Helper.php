<?php
/**
 * Created by PhpStorm.
 * User: terry
 * Date: 2017-10-21
 * Time: 12:42 PM
 */

if (!function_exists('array_map_assoc')) {
    function array_map_assoc(callable $f, array $a)
    {
        return array_column(array_map($f, array_keys($a), $a), 1, 0);
    }
}

if (!function_exists('to_array')) {
    function to_array($data)
    {
        if (!is_array($data) && !is_object($data)) {
            return $data;
        }
        return json_decode(json_encode($data), true);
    }
}