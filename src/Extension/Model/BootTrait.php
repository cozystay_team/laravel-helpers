<?php

namespace Galaxy\Helpers\Extension\Model;

use Galaxy\Helpers\Exception\ExceptionMapping;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Webpatser\Uuid\Uuid;

trait BootTrait
{
    /**
     * override Laravel Model boot() method
     */
    public static function boot()
    {
        parent::boot();

        /**
         * saving event
         */
        static::saved(function ($model) {
            //T@C get rid of the encoded fields
            if ($model instanceof BaseModel) {
                if ($model->processed()) {
//                    _cz_log('refresh');
                    $model->refresh();
                }
            }

        });

        static::saving(function (Model $model) {

            if (!$model->{$model->getKeyName()}) {
                $model->{$model->getKeyName()} = Uuid::generate()->string;
            }

            if ($password = $model->{'password'}) {
                if (app('hash')->needsRehash($password)) {
                    $model->{'password'} = app('hash')->make($password);
                }
            }

            if ($model instanceof EncodableAndTranslatableInterface) {
                $model->beforeSave();
                $model->setProcessed(true);
                if ($model instanceof BaseModel) {
                    self::processTranslate($model);
                    self::processEncodes($model);
                }
            }


            return true;
        });
        /**
         * deleted event
         */
        static::deleting(function (Model $model) {
            try {
                DB::table('translations')->where(
                    [
                        'related_id' => $model->{$model->getKeyName()},
                        'namespace' => get_class($model),
                    ]
                )->delete();
            } catch (\Exception $e) {
                throw $e;
            }
        });
    }

    /**
     * Register a loaded model event with the dispatcher.
     *
     * @param  \Closure|string $callback
     * @return void
     */
    public static function loaded($callback)
    {
        static::registerModelEvent('loaded', $callback);
    }

    private static function processEncodes(BaseModel $model)
    {
        if (!$encodeDecodeFields = $model->encodeDecodeFields) {
            return true;
        }

        foreach ($encodeDecodeFields as $item) {
            if (!empty($model->{$item})) { // data is not empty
                if (
                    (is_array($model->{$item})
                        ||
                        is_object($model->{$item})) // it must be ether array or object. NOTE: a string won't be valid input, so don't encode your data before sending
                    &&
                    $encodedData = json_encode($model->{$item}) // it must be encodable
                ) {
                    $model->{$item} = $encodedData;
                } else if (is_string($model->{$item})) {
                    $model->{$item} = json_encode(json_decode($model->{$item}));
                } else { // otherwise
                    _cz_log("encoding error 2: $item -> {$model->{$item}}");
                    throw ExceptionMapping::exceptionFromErrorCode(40001, "Invalid JSON data (encoding): $item -> {$model->{$item}}");
                }
            } else { //else the data is empty, then don't encode it and set it to be null (in case of empty array)
                $model->{$item} = "{}";
            }
        }
    }

    private static function processTranslate(BaseModel $model)
    {
        if (!$translationFields = $model->translationFields) {
            return true;
        }

        if (!$model->allLanguagesTranslations) {
            $model->allLanguagesTranslations = [];
        }

        $language = Request::header('coz-lang', 'en');

        $dirtyFields = $model->getDirty();

        foreach ($translationFields as $field) {
            unset($model->{$field});

            $model->allLanguagesTranslations[$field] = $model->allLanguagesTranslations[$field] ?? [];
//            _cz_log("current all translations", $model->allLanguagesTranslations, $field);
            if (!$newTrans = array_get($dirtyFields, $field)) {
                if($newTrans!==""){
                    continue;
                }
            }

            $currentTrans = array_values($model->allLanguagesTranslations[$field]);

            //T@C: this is not good, we are search new translation values in previous translations and only
            // add if no match is found. We should have a better way of doing this.
            if (!in_array($newTrans, $currentTrans)) {
                array_set($model->allLanguagesTranslations, "$field.$language", $newTrans);
                array_set($model->allLanguagesTranslations, "$field.modified", true);
            }
        }

//        _cz_log("all translateData", $model->allLanguagesTranslations);

        foreach ($model->allLanguagesTranslations as $fieldName => $fieldValue) {
            if (!array_get($fieldValue, 'modified')) {
                continue;
            }
            $subData = [
                'lang' => $language,
                'namespace' => get_class($model),
                'related_id' => $model->{$model->getKeyName()},
                'key' => $fieldName,
                'value' => $fieldValue[$language]
            ];
//            _cz_log("current language", $language, $subData);

            DB::table('translations')
                ->updateOrInsert(
                    array_except($subData, 'value'),
                    array_only($subData, 'value'));

        }


    }

    /**
     * Get the observable event names.
     *
     * @return array
     */
    public function getObservableEvents()
    {
        return array_merge(parent::getObservableEvents(), array('loaded'));
    }

    /**
     * Create a new model instance that is existing.
     *
     * @param  array $attributes
     * @param  $connection
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function newFromBuilder($attributes = array(), $connection = NULL)
    {
        $instance = parent::newFromBuilder($attributes);

        $instance->fireModelEvent('loaded', false);

        return $instance;
    }

    public function loadFields()
    {
        //decode payload fields

        $this->decode();

        $this->translate();

        $this->setProcessed(false);


    }

    private function decode()
    {
        $model = $this;
        // decode fields that need to be auto-decoded
        if (!$model->encodeDecodeFields) {
            return;
        }
        foreach ($model->encodeDecodeFields as $item) {
            if (empty($model->{$item})) {
                continue;
            }
            $model->{$item} = json_decode($model->{$item}, true);
            // return whatever json_decode outputs, if $item is invalid json, will just return null
        }
    }

    /**
     * Retrieve corresponding locale data from translations table, and merge locale with Model for GET requests
     */
    private function translate()
    {

        $model = $this;

        if (!$model->translationFields) {
            return;
        }
        $header = str_replace('_', '-', strtolower(Request::header('coz-lang', 'en')));

        $orderedLocales = ['en', 'zh-cn', 'zh-tw'];

        if (!in_array($header, $orderedLocales)) {
            throw  ExceptionMapping::exceptionFromErrorCode(40018);
        }
        array_unshift($orderedLocales, $header);

        $orderedLocales = array_unique($orderedLocales);


        $orderedLocalesImpl = implode("', '", $orderedLocales);
        $orderedLocalesImpl = "'$orderedLocalesImpl'";

//        _cz_log($orderedLocalesImpl);

        if(!$localeValues = DB::table('translations')->where(
            [
                'related_id' => $model->{$model->getKeyName()},
                'namespace' => get_class($model),
            ]
        )->orderByRaw("FIELD( lang , $orderedLocalesImpl) ASC")->get()->toArray()){
            return;

        }

        // create translationFields only, null if no record found in DB
        foreach ($model->translationFields as $field) {
            $model->{$field} = null;
        }

        $model->allLanguagesTranslations = [];


        foreach ($localeValues as $item) {
            // because $locales is already sorted, we can loop and
            // firstly use highest priority locale item to assign value to the $model
            // if the higher priority has no value on the field, then we check the lowe priority ones.
            array_set($model->allLanguagesTranslations, "{$item->key}.{$item->lang}", $item->value);
            if ($model->{$item->key}) continue;
            $model->{$item->key} = $item->value;
        }
//        _cz_log("load translate", $model->allLanguagesTranslations);

    }
}