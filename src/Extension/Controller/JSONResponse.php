<?php
/**
 * Created by PhpStorm.
 * User: terrywang
 * Date: 2017/5/10
 * Time: 15:12
 */

namespace Galaxy\Helpers\Extension\Controller;


trait JSONResponse
{
    use ResponseCodes;

    protected function respond($status, $data = [])
    {
        return response()->json($data, $status);
    }


    /**
     * @param int $successCode
     * @param null $data
     * @param bool $merge
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    protected function successRespond(int $successCode, $data = null, bool $merge = false)
    {

        $headers = $this->responseCode($successCode);
        $return = ['code' => 0];
        if (isset($data) ) {
            if($merge){
                $return = array_merge($return,$data);
            }else{
                $return['data'] = $data;
            }
        } else {
            $return['success'] = true;
        }

        $status = (int)($headers['code'] / 100);
        return response()->json($return, $status)
            ->header('Response-Code', $headers['code'])
            ->header('Response-Message', $headers['message']);

    }
}