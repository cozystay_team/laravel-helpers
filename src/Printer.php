<?php

use Galaxy\Helpers\Colors;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;


/**
 * Created by PhpStorm.
 * User: terrywang
 * Date: 2017/5/1
 * Time: 18:16
 *
 */

// NOTE: _console printing methods are used for phpunit testing while dd()/var_dump() not working/terminating.

if (!function_exists('_consoleLogWithColor')) {

    /**
     * a helper method to print colored msg to console
     * @param $msg
     * @param $color
     */

    function _consoleLogWithColor($msg, $color = Colors::WHITE)
    {
        echo "\n$color ";
        print_r($msg);
        echo Colors::END_COLORING;
    }
}


if (!function_exists('_consoleLog')) {

    /**
     * a helper method to print all msg to console with given
     * @param array $msgs
     * @internal param $msg
     * @internal param bool $success
     * @internal param $color
     */

    function _consoleLog(...$msgs)
    {
        $color = $msgs[count($msgs) - 1];
        for ($i = 0; $i < count($msgs) - 2; $i++)
            _consoleLogWithColor($msgs[$i], $color);
    }
}

if (!function_exists('_consoleError')) {

    /**
     * a helper method to print error msg to console in red color
     * @param array $msgs
     * @internal param $msg
     */
    function _consoleError(...$msgs)
    {
        foreach ($msgs as $msg)
            _consoleLogWithColor($msg, Colors::RED);
    }
}

if (!function_exists('_consoleWarning')) {

    /**
     * a helper method to print warning msg to console in Yellow color
     * @param array $msgs
     * @internal param $msg
     * @internal param bool $success
     * @internal param $color
     */
    function _consoleWarning(...$msgs)
    {
        foreach ($msgs as $msg)
            _consoleLogWithColor($msg, Colors::YELLOW);
    }
}

if (!function_exists('_consoleNote')) {

    /**
     * a helper method to print error msg to console in Cyan color
     * @param array $msgs
     * @internal param $msg
     * @internal param bool $success
     * @internal param $color
     */
    function _consoleNote(...$msgs)
    {
        foreach ($msgs as $msg)
            _consoleLogWithColor($msg, Colors::CYAN);
    }

}


if (!function_exists('_cz_log')) {

    function _log_to_file(array $data, $location)
    {
        $orderLog = new Logger("GLXY");

        $output = "[%datetime%] %message%\n";
        $formatter = new LineFormatter($output);
        $handler = new RotatingFileHandler($location);
        $handler->setFormatter($formatter);
        $orderLog->pushHandler($handler);

        $bt = debug_backtrace();
        array_shift($bt);
        $caller = array_shift($bt);

        $file = $caller['file'];
        $line = $caller['line'];
        $orderLog->info("$file: $line");

        $orderLog = new Logger("GLXY");

        $output = "%message% \n";
        $formatter = new LineFormatter($output);
        $handler = new RotatingFileHandler($location);
        $handler->setFormatter($formatter);
        $orderLog->pushHandler($handler);
    }

    /**
     * Reference Monolog RFC 5424
     * Code    Severity
     * 0       Emergency: system is unusable
     * 1       Alert: action must be taken immediately
     * 2       Critical: critical conditions
     * 3       Error: error conditions
     * 4       Warning: warning conditions
     * 5       Notice: normal but significant condition
     * 6       Informational: informational messages
     * 7       Debug: debug-level messages
     * @param array $messages
     * @param int $level
     * @param int $iteration
     */
    function _cz_logger(array $messages, int $level, int $iteration = 1)
    {
        $first = array_shift($messages);

        if (is_array($first)) {
            $firstStr = json_encode($first, JSON_UNESCAPED_UNICODE);
        } else if (is_object($first)) {
            $firstStr = json_encode($first, JSON_UNESCAPED_UNICODE);
        } else {
            $firstStr = strval($first);
        }

        $bt = debug_backtrace();

        for ($i = 0; $i < $iteration; $i++) {
            $caller = array_shift($bt);
        }

        $file = $caller['file'];
        $line = $caller['line'];
        array_push($messages, "@$file:$line");

        switch ($level) {
            case 7:
                Log::debug($firstStr, $messages);
                break;
            case 6:
                Log::info($firstStr, $messages);
                break;
            case 5:
                Log::notice($firstStr, $messages);
                break;
            case 4:
                Log::warning($firstStr, $messages);
                break;
            case 3:
                Log::error($firstStr, $messages);
                break;
            case 2:
                Log::critical($firstStr, $messages);
                break;
            case 1:
                Log::alert($firstStr, $messages);
                break;
            case 0:
                Log::emergency($firstStr, $messages);
                break;
        }
    }

    ;
    function _cz_logger_debug(...$msg)
    {
        _cz_logger($msg, 7, 2);
    }

    function _cz_logger_info(...$msg)
    {
        _cz_logger($msg, 6, 2);
    }

    function _cz_logger_notice(...$msg)
    {
        _cz_logger($msg, 5, 2);
    }

    function _cz_logger_warning(...$msg)
    {
        _cz_logger($msg, 4, 2);
    }

    function _cz_logger_error(...$msg)
    {
        _cz_logger($msg, 3, 2);
    }

    function _cz_logger_critical(...$msg)
    {
        _cz_logger($msg, 2, 2);
    }

    function _cz_logger_alert(...$msg)
    {
        _cz_logger($msg, 1, 2);
    }

    function _cz_logger_emergency(...$msg)
    {
        _cz_logger($msg, 0, 2);
    }

    /**
     * debug log
     * @param array $messages
     */

    function _cz_log(...$messages)
    {
        _cz_logger($messages, 7, 2);
    }


    /**
     * access log
     * access logs
     * @param array ...$messages
     */
    function __a(...$messages)
    {
        _cz_logger($messages, 6, 2);
    }

}