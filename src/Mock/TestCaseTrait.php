<?php
/**
 * Created by PhpStorm.
 * User: qiyuzhao
 * Date: 2017-06-19
 * Time: 9:53 AM
 */

namespace Galaxy\Helpers\Mock;

use App\InterSystem\Actions\BaseAction;
use GuzzleHttp\Psr7\Response;
use Mockery;
use Mockery\MockInterface;

trait TestCaseTrait
{

    /**
     * helper function to get difference between 2 arrays by key => value pairs
     * @param $arr1
     * @param $arr2
     * @return array
     */
    public function array_diff_restrict($arr1, $arr2)
    {
        $arr3 = [];
        foreach ($arr1 as $key => $value) {
            if (array_has($arr2, $key)) {
                $compareValue = array_get($arr2, $key);
                if ($compareValue !== $value) {
                    $arr3[$key] = [
                        $arr1[$key],
                        $arr2[$key],
                    ];
//                    _consoleWarning($key,  $arr1[$key], $arr2[$key]);
                }
                unset($arr2[$key]);
            } else {
                $arr3[$key] = $arr1[$key];
            }
        }
        return array_merge($arr3, $arr2);
    }

    public function checkIsSoftDeletedInDatabase($table, $data)
    {
        $deletedAt = $data;
        $deletedAt['deleted_at'] = null;
        return $this->seeInDatabase($table, $data)
            ->notSeeInDatabase($table, $deletedAt);
    }

    public function checkIsDeletedInDatabase($table, $data)
    {
        return $this->notSeeInDatabase($table, $data);
    }

    /**
     * load mockery response
     * @param MockInterface $client
     * @param string $planet
     * @param array $endpointDefinition
     * @param array $payloads
     * @param int $times
     * @param int $code
     */
    protected function loadMockResponse(MockInterface &$client, string $planet, array $endpointDefinition, array $payloads = [], $responseString = null, int $times = 1, int $code = 200)
    {
        if ($responseString) {
            $stream = $responseString;
        } else {
            $stream = $this->loadResponseJsonStream($planet, $endpointDefinition, $code);
        }
        $responses = [];
        while ($times-- > 0) {
            $responses [] = new Response($code, ['Content-Type' => 'application/json'], $stream);
        }
        $path = BaseAction::formPath($endpointDefinition['endPoint'], $payloads);
//        _consoleNote("loadMockResponse - path: " . $path);

        $client->shouldReceive('request')->with($endpointDefinition['method'], $path, Mockery::any())->andReturnValues($responses);
    }

    /**
     * create a json structure for phpunit test.
     *
     * @param array $arr the array must be standard json response from laravel. (numeric array with key will be treated as repeating array)
     * @return array
     */
    protected function JsonStructureFromArray(array $arr)
    {
        $result = [];
        $keys = array_keys($arr);
        if (is_numeric($keys[0])) {
            // if the key is numeric, then the array is a repeating array, use "*" to indicate repeating.
            $result["*"] = $this->JsonStructureFromArray($arr[0]);
        } else {
            // else
            foreach ($arr as $key => $value) {
                if (is_array($value)) {
                    $result[$key] = $this->JsonStructureFromArray($value);
                } else {
                    $result[] = $key;
                }
            }
        }
        return $result;
    }

    /**
     * load Json stream from local file
     * @param string $planet
     * @param array $endpointDefinition
     * @param int $code defualt to be 200
     * @return string
     */
    protected function loadResponseJsonStream(string $planet, array $endpointDefinition, int $code = 200)
    {
//        _consoleWarning("params: ", $planet, $endpointDefinition);
        $planetName = strtoupper((new \ReflectionClass($planet))->getShortName());
        $endpointDefinition['endPoint'] = strtoupper($endpointDefinition['endPoint']);
        $endpointDefinition['endPoint'] = str_replace('/', '_', $endpointDefinition['endPoint']);
        $path = "responses/{$planetName}_{$endpointDefinition['method']}_{$endpointDefinition['endPoint']}_$code.json";
//        _consoleWarning("path: " . $path);
        try {
//            _consoleNote("Reading File: $path");
            $stream = file_get_contents($path);
            return $stream;
        } catch (\Exception $e) {
            _consoleError("Error: Can't find file -> $path \n Exception: {$e->getMessage()}");
            return null;
        }
    }

    /**
     * get data structure/schema
     * @param string $planet
     * @param array $endpointDefinition
     * @param int $code
     * @return array
     */
    protected function getArrayStructure(string $planet, array $endpointDefinition, int $code = 200)
    {
        $stream = $this->loadResponseJsonStream($planet, $endpointDefinition, $code);
        $structure = $this->JsonStructureFromArray(json_decode($stream, true));
        return $structure;
    }

    /**
     * generate a Laravel standard route with given vars.
     * @param string $prefix
     * @param array $endpoint
     * @param array $endPointExtra
     * @return string
     */
    protected function formUrl(string $prefix, array $endpoint, array $endPointExtra = [])
    {
        $path = $prefix . $endpoint['endPoint'];
        foreach ($endPointExtra as $value) {
            $path = preg_replace("/({[^}]+})/", $value, $path, 1);
        }
        return $path;
    }
}