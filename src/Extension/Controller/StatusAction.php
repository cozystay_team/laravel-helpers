<?php
/**
 * Created by PhpStorm.
 * User: terrywang
 * Date: 2017/4/27
 * Time: 23:32
 */

namespace Galaxy\Helpers\Extension\Controller;

use Galaxy\Helpers\Exception\ExceptionMapping;
use Galaxy\Helpers\Extension\Model\StatusModel;

trait StatusAction
{
    use RESTActions;

    /**
     * @param $id
     * @param $action
     * @param string $field
     * @return \Illuminate\Http\JsonResponse
     * @internal param Model $object
     */

    public function action($id, $action, $field='status')
    {
        $m = self::MODEL;
        $object = $m::find($id);
        if (!$object) {
            throw ExceptionMapping::exceptionFromErrorCode(40401, "could not found object for $id, model $m");

        }
        $object->doAction( $action,$field);
        return $this->successRespond(202, $object->fresh());

    }


}