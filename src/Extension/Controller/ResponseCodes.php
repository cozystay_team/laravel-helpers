<?php

namespace Galaxy\Helpers\Extension\Controller;

/**
 * Class ResponseCodes
 * @package App\Http\Controllers\Responses
 */
trait ResponseCodes
{

    // cozystay success codes

    private $ResponseCodes = array(
        20000 => "OK",
        20001 => "Request successful, but 0 record changed (your request data might be incorrect)",
        20002 => "No listing Request created, but listing has been updated.",
        20100 => "Created",
        20200 => "Accepted",
        20400 => "No Content",
    );

    /**
     * Generate standard Cozystay error code
     * @param $code : full error code 40401. or standard error like 404 -> instead of 40400
     * @return array
     */
    public function responseCode(int $code)
    {
        $code = $code < 10000 ? $code * 100 : $code;

        $message = array_get($this->ResponseCodes, $code);
        if (!$message) {
            $code = 20000;
            $message = $this->ResponseCodes[$code];
        }


        return compact('code', 'message');
    }

}