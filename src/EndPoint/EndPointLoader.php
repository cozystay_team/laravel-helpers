<?php
/**
 * Created by PhpStorm.
 * User: terrywang
 * Date: 16/06/2017
 * Time: 1:40 PM
 */

namespace Galaxy\Helpers\EndPoint;


use ReflectionClass;
use Route;

class EndPointLoader
{

    /**
     * init Routes from given planet
     * @param string $planet
     */
    public static function initRoutesFromEndPoint(string $planet)
    {

        $endPoints = PermissionGenerator::generateEndPointList($planet);

        foreach ($endPoints as $endPoint) {
            // filter out const like PREFIX
            if (is_array($endPoint)
                && array_key_exists('method', $endPoint)
                && array_key_exists('endPoint', $endPoint)
                && array_key_exists('controller', $endPoint)
            ) {


                $attributes = array_only($endPoint, ['namespace', 'middleware']);

                Route::group($attributes, function () use ($endPoint) {
                    switch (strtoupper($endPoint['method'])) {
                        case 'GET' :
                            Route::get($endPoint['endPoint'], $endPoint['controller']);
                            break;
                        case 'PUT' :
                            Route::put($endPoint['endPoint'], $endPoint['controller']);
                            break;
                        case 'POST' :
                            Route::post($endPoint['endPoint'], $endPoint['controller']);
                            break;
                        case 'PATCH' :
                            Route::patch($endPoint['endPoint'], $endPoint['controller']);
                            break;
                        case 'DELETE' :
                            Route::delete($endPoint['endPoint'], $endPoint['controller']);
                            break;
                        case 'OPTIONS' :
                            Route::options($endPoint['endPoint'], $endPoint['controller']);
                    }

                });

            }
        }
    }

    public static function initLumenRoutesFromEndPoint(string $planet, $app)
    {
        // register routes based on Planet definition.
        $oClass = new ReflectionClass($planet);
        $endPoints = $oClass->getConstants();
        foreach ($endPoints as $endPoint) {
            // filter out const like PREFIX
            if (is_array($endPoint)
                && array_key_exists('method', $endPoint)
                && array_key_exists('endPoint', $endPoint)
                && array_key_exists('controller', $endPoint)
            ) {

                $options = array_only($endPoint, ['namespace', 'middleware']);

                // note: we don't support Constraints for now.

                $app->group($options, function () use ($app, $endPoint) {
                    switch (strtoupper($endPoint['method'])) {
                        case 'GET' :
                            $app->get($endPoint['endPoint'], $endPoint['controller']);
                            break;
                        case 'PUT' :
                            $app->put($endPoint['endPoint'], $endPoint['controller']);
                            break;
                        case 'POST' :
                            $app->post($endPoint['endPoint'], $endPoint['controller']);
                            break;
                        case 'PATCH' :
                            $app->patch($endPoint['endPoint'], $endPoint['controller']);
                            break;
                        case 'DELETE' :
                            $app->delete($endPoint['endPoint'], $endPoint['controller']);
                            break;
                        case 'OPTIONS' :
                            $app->options($endPoint['endPoint'], $endPoint['controller']);
                    }
                });
            }
        }
    }


}