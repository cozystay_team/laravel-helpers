<?php

namespace Galaxy\Helpers\EndPoint;

trait RuleGenerator
{
    /**
     * helper function of laravel required_without_all rule
     * @param $originRules array
     * @return array
     */
    public static function requiredWithoutAll(array $originRules)
    {
        $keys = array_keys($originRules);

        $newRules = [];

        foreach ($originRules as $key => $rule) {
            $requiredKeys = array_diff($keys, [$key]);
            if (strlen($rule) > 0) {
                $rule .= "|required_without_all:" . implode(",", $requiredKeys);
            } else {
                $rule = "required_without_all:" . implode(",", $requiredKeys);
            }
            $newRules[$key] = $rule;
        }


        return $newRules;
    }
}