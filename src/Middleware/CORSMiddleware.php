<?php

namespace Galaxy\Helpers\Middleware;
use Illuminate\Http\Request;

/**
 * Created by PhpStorm.
 * User: terrywang
 * Date: 2017/5/9
 * Time: 23:08
 */
class CORSMiddleware
{
    public function handle(Request $request, \Closure $next)
    {
        if($request->method()=='OPTIONS'||$request->method()=='HEAD'){
            $response = response()->make();
        }else{
            $response = $next($request);
        }

        $response->header('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE');
        $response->header('Access-Control-Allow-Headers', $request->header('Access-Control-Request-Headers'));
        $response->header('Access-Control-Allow-Origin', '*');

        return $response;
    }

}