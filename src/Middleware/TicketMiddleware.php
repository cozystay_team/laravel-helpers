<?php
/**
 * Created by PhpStorm.
 * User: terrywang
 * Date: 22/06/2017
 * Time: 9:38 PM
 */

namespace Galaxy\Helpers\Middleware;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TicketMiddleware
{
    public function handle(Request $request, \Closure $next)
    {
//        DB::beginTransaction();
//
//        try {
//
//            $ticket = $request->header('GLXY-Ticket-id');
//
//            if (!$ticket) {
                $response = $next($request);
//            } else {
//                if ($ticketData = DB::table('tickets')->find($ticket)) {
//                    $result = $ticketData['value'];
//                    $response = response()->json(json_decode($result));
//                } else {
//                    $response = $next($request);
//
//                    DB::table('tickets')
//                        ->insert([
//                            'id' => $ticket,
//                            'value' => $response->content(),
//                            'code' => $response->status(),
//                            "created_at" => Carbon::now(), # \Datetime()
//                            "updated_at" => Carbon::now(),  # \Datetime()
//                        ]);
//                }
//            }
//
//            DB::commit();
//
//        } catch (\Exception $e) {
//            DB::rollback();
//            throw $e;
//
//        }

        return $response;
    }

}