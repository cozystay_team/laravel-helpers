<?php

namespace Galaxy\Helpers;

/**
 * Class Colors. Enum of Colors.
 * @package Tests
 */
class Colors
{
    const END_COLORING = "\e[0m";
    const WHITE = "\e[37m";
    const BLACK = "\e[30m";
    const GREEN = "\e[32m";
    const RED = "\e[31m";
    const BLUE = "\e[34m";
    const CYAN = "\e[36m";
    const MAGENTA = "\e[45m";
    const YELLOW = "\e[1;33m";
    const PURPLE = "\e[0;35m";
    const BROWN = "\e[33m";
    const DARK_GRAY = "\e[1;30m";
    const LIGHT_BLUE = "\e[1;34m";
    const LIGHT_GREEN = "\e[1;32m";
    const LIGHT_CYAN = "\e[1;36m";
    const LIGHT_RED = "\e[1;31m";
    const LIGHT_PURPLE = "\e[1;35m";
    const  LIGHT_GRAY = "\e[37m";
}