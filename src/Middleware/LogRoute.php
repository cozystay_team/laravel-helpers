<?php

namespace Galaxy\Helpers\Middleware;
use Closure;

class LogRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        _cz_log( "Request: {$request->method()} {$request->path()} from {$request->ip()} with Params:", $request->all());

        return $next($request);
    }
}
