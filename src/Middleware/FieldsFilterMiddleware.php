<?php

namespace Galaxy\Helpers\Middleware;

use Illuminate\Http\Request;


/**
 * Created by PhpStorm.
 * User: terrywang
 * Date: 29/06/2017
 * Time: 1:43 PM
 */
class FieldsFilterMiddleware
{

    public function handle(Request $request, \Closure $next, ...$param)
    {

        $request->query->replace($this->cleanParameter($request->query->all(), $param));

        if ($request->isJson()) {
            $request->json()->replace($this->cleanParameter($request->json()->all(), $param));
        } else {
            $request->request->replace($this->cleanParameter($request->request->all(), $param));
        }

        return $next($request);


    }

    private function cleanParameter(array $parameters, $except_keys)
    {

        return array_except($parameters, $except_keys);

    }
}