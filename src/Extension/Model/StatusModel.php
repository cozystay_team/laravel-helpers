<?php

namespace Galaxy\Helpers\Extension\Model;

/**
 * Created by PhpStorm.
 * User: terrywang
 * Date: 2017/4/28
 * Time: 10:28
 */


use Illuminate\Validation\Rule;

class StatusModel extends BaseModel
{

    /**
     * $statusTable =
     * ['status1'   =>
     *      [
     *          'action1'=>'status2',
     *          'action2'=>'status3',
     *      ]
     *  'status2'   =>
     *      [
     *          'action3'=>'status3',
     *          'action4'=>'status4',
     *      ]
     * ]
     */

    protected $statusTable;

    protected $status;

    protected $errors;

    public function doAction($action, $field = 'status')
    {

        $this->doActionNoCommit($action, $field);
        $this->save();
        return $this;
    }

    public function doActionNoCommit($action, $statusField = 'status')
    {
        $fields = array_merge($this->toArray(), ['action' => $action]);
        $fields = array_except($fields, $statusField);
        $this->validate($fields);
        $this->fill([
            $statusField => array_get($this->statusTable, "{$this->getAttributes()[$statusField]}.$action")
        ]);
        return $this;
    }


    public function getRules($data, $model)
    {
        $status = $model->getAttributes()['status'];
        $rules = array_merge($model->rules,
            [
                'status' => [
                    Rule::in(array_values($model->statusTable[$status])),
                ],
                'action' => [
                    Rule::in(array_keys($model->statusTable[$status])),
                ]
            ]
        );
        return $rules;

    }

}