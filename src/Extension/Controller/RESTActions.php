<?php
/**
 * Created by PhpStorm.
 * User: terrywang
 * Date: 2017/5/9
 * Time: 23:51
 */

namespace Galaxy\Helpers\Extension\Controller;


use Galaxy\Helpers\Exception\ExceptionMapping;
use Illuminate\Http\Request;

trait RESTActions
{

    use PageAndSort, JSONResponse;

    public function all(Request $request)
    {

        $m = self::MODEL;
        $query = $m::query();
        $ids = $request->query('ids');
        $conditions = $request->query('query');
        $relationConditions = $request->query('relationQuery');
        $with = $request->query('with');

        if ($ids) {
            $ids = array_map('trim', explode(',', $ids));
            $query = $query->whereIn('id', $ids);
        }
        if ($conditions) {
            $query = $this->addConditions($query, $conditions);
        }
        if ($relationConditions) {
            $query = $this->addRelationalConditions($query, $relationConditions);
        }
        if ($with) {
            $query->with(array_map('trim', explode(',', $with)));
        }
        $total = $query->count();

        $query = $this->addPagingAndSort($query, $request);
        return $this->respond(200, $this->dataWithSkipAndList($query, $request, $total));

    }

    public function get(Request $request, $id)
    {
        $m = self::MODEL;
        $with = $request->query('with');
        $enum = $request->query('enum');
        $query = $m::where(['id' => $id]);
        if ($with) {
            $query->with(explode(',', $with));
        }
        $model = $query->first();
        if (!$model) {
            throw ExceptionMapping::exceptionFromErrorCode(40401);
        }
        if($enum) {
            $model->attachEnumerations($enum);
        }
        $modelData = $model->toArray();

        return $this->successRespond(200, $modelData);
    }

    public function add(Request $request)
    {
        $m = self::MODEL;
        $object = new $m;
        $object->validate($request->except(['with']));
        $object = $m::create($request->except(['with']));

        $with = $request->get('with');
        if ($with){
            $object = $object->load($with);
        }

        return $this->successRespond(201, $object);
    }

    public function edit(Request $request, $id)
    {
        $m = self::MODEL;
        $object = $m::find($id);
        if (is_null($object)) {
            throw ExceptionMapping::exceptionFromErrorCode(40401);
        }
        $object->validate(array_merge($object->toArray(), $request->except(['with'])));
        $object->update($request->except(['with']));

        $with = $request->get('with');
        if ($with){
            $object = $object->load($with);
        }else{
            $object->refresh();
        }

        return $this->successRespond(200, $object);
    }


    public function remove(Request $request, $id)
    {
        $m = self::MODEL;
        if (is_null($obj = $m::find($id))) {
            throw ExceptionMapping::exceptionFromErrorCode(40401);
        }
        $obj->delete();
        return $this->successRespond(200);
    }

    /**
     * remove all by ids
     * ?ids=id1,id2,id3
     * @param Request $request
     * @return $this
     * @internal param $ids
     */
    public function removeAll(Request $request)
    {
        $m = self::MODEL;
        $ids = $request->input('ids');
        if(!$ids) {
            throw ExceptionMapping::exceptionFromErrorCode(40000);
        }
        $ids = explode(',', $ids);
        $m::whereIn('id', $ids)->delete();
        return $this->successRespond(200);
    }
    /**
     * updateOrCreate all by array of data.
     * if "id" is not given, then the function will create new record. otherwise, the function will update it
     * "data" => [
     *              ["id" => "id1", "name" => "data1" ...],
     *              ["id" => "id2", "description" => "data2" ...],
     *              ["description" => "data2" ...],
     *          ]
     * @param Request $request
     * @param $id
     * @return $this
     */
    public function updateOrCreateAll(Request $request)
    {
        $m = self::MODEL;
        $data = $request->input('data');
        if(!$data || !is_array($data)) {
            throw ExceptionMapping::exceptionFromErrorCode(40000);
        }
        $result = [];
        foreach ($data as $item) {
            // TODO: validation
            $collection = $m::updateOrCreate(
                ['id' => array_get($item, 'id', null)],
                array_except($item, 'id')
            );
            $result[] = $collection->fresh();
        }
        return $this->successRespond(200, $result);
    }
}
