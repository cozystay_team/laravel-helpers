<?php
/**
 * Created by PhpStorm.
 * User: terrywang
 * Date: 2017/5/10
 * Time: 10:47
 */

namespace Galaxy\Helpers\Extension\Controller;


use Galaxy\Helpers\Exception\ExceptionMapping;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as RequestFacade;

use Mockery\Exception;

trait PageAndSort
{

    public static $DEFAULT_SKIP = 0;
    public static $DEFAULT_LIMIT = 10;

    /**
     * http://xxxxxxxxxx.xx/api?order=id,created_at,name:desc
     *      :desc is not required, by default, it will sort with ascending order
     * @param Builder $query
     * @param Request $request
     * @return Builder
     */
    protected function addPagingAndSort(Builder $query, Request $request)
    {

        $skip = $request->query('skip') ?: $this::$DEFAULT_SKIP;
        $limit = $request->query('limit') ?: $this::$DEFAULT_LIMIT;

        $orderQueryArray = $request->query('order') ? explode(',', $request->query('order')) : [];

        $sortClauseList = array_map(function ($sort) {
            $sortArray = explode(":", $sort);
            $sortKey = $sortArray[0];
            $ascOrDesc = count($sortArray) > 1 ? $sortArray[1] : "asc";
            return ["key" => $sortKey, "value" => $ascOrDesc];
        }, $orderQueryArray);

        $resultQuery = $query;
        foreach ($sortClauseList as $oneClause) {
            $resultQuery = $resultQuery->orderBy($oneClause["key"], $oneClause["value"]);
        }
        $resultQuery->orderBy('updated_at', 'desc');

        $resultQuery = $resultQuery->skip($skip)->limit($limit);

        return $resultQuery;
    }

    protected function dataWithSkipAndList(Builder $query, Request $request, $total)
    {

        $skip = $request->query('skip') ?: $this::$DEFAULT_SKIP;
        $limit = $request->query('limit') ?: $this::$DEFAULT_LIMIT;

        $data = $query->get();


        return
            [
                'code' => 0,
                'data' => $data->all(),
                'skip' => (int)$skip,
                'limit' => (int)$limit,
                'total' => (int)$total
            ];
    }

    /**
     * conditions format:
     * http://xxxxxxxxxx.xx/api?query=time,>,1232343434|room_count,>,3|id,in,111~222~333|old_id,not,111~222~333
     * <host>/api?query=<attribute>,[<operator>,]<value>[|<attribute>,[<operator>,]<value>]
     * 2 formats:
     *      key,value
     *      key,operator,value
     * operators: =, >, <, >=, <=, <>, in, not, like
     * @param Builder $query
     * @param $conditions
     * @return Builder
     */
    protected function addConditions(Builder $query, $conditions): Builder
    {
        if ($conditions) {
            $m = self::MODEL;
            $model = new $m;
            $conditions = explode('|', $conditions);
            $translationsFieldsConditions = [];
            foreach ($conditions as $item) {
                $where = explode(',', $item);
                if (strtolower(last($where)) == 'null') {
                    $where[count($where) - 1] = null;
                }
                if (count($where) > 0 && in_array($where[0], $model->translationFields)) {
                    $translationsFieldsConditions[] = $where;
                } else {
                    $query = $this->_applyCondition($query, $where);
                }
            }
            if ($translationsFieldsConditions) {
                $query = $this->_addTranslationFieldsQuery($query, $translationsFieldsConditions, $m);
            }
        }
//        _cz_log($conditions, $query->toSql());
        return $query;
    }

    /**
     * conditions format:
     * 1 relation:
     *      http://xxxxxxxxxx.xx/api?relationQuery=location:city,like,vanco|province,=,British Columbia
     *      <host>/api?relationQuery=<relation>:<attribute>,[<operator>,]<value>[|<attribute>,[<operator>,]<value>]
     * more than 1 relations:
     *      http://xxxxxxxxxx.xx/api?relationQuery[]=location:city,like,vanco|province,=,British Columbia&relationQuery[]=review:rating,>,3
     *      <host>/api?relationQuery[]=<relation>:<attribute>,[<operator>,]<value>[|<attribute>,[<operator>,]<value>][&relationQuery[]=<relation>:<attribute>,[<operator>,]<value>[|<attribute>,[<operator>,]<value>]]
     * 3 formats:
     *      attribute,value
     *      attribute,operator,value
     *      count(attribute),operator,value
     * operators: =, like, >, <, >=, <=, <>, in, not
     * @param Builder $query
     * @param $conditions
     * @return Builder
     */
    protected function addRelationalConditions(Builder $query, $conditions): Builder
    {
        if (!$conditions) {
            return $query;
        }
        if (is_array($conditions)) {
            foreach ($conditions as $item) {
                $query = $this->addRelationalConditions($query, $item);
            }
        } elseif (is_string($conditions)) {
            $conditions = explode(':', $conditions);
            $relation = array_get($conditions, '0');
            $conditions = array_get($conditions, '1');
            if (!$relation || !$conditions) { // it must contain 2 fields: relation & conditions
                return $query;
            }

            $query->whereHas($relation, function ($query) use ($conditions) {
                $conditions = explode('|', $conditions);
                foreach ($conditions as $item) {
                    $where = explode(',', $item);
                    if (strtolower(last($where)) == 'null') {
                        $where[count($where) - 1] = null;
                    }
                    $query = $this->_applyCondition($query, $where);
                }
            });
        }

        return $query;
    }

    private function _addTranslationFieldsQuery(Builder $query, array $conditions, string $model): Builder
    {
//        $locale = str_replace('_', '-', strtolower(RequestFacade::header('coz-lang', 'en')));
//        $orderedLocales = ['en', 'zh-cn', 'zh-tw'];
//        if (!in_array($locale, $orderedLocales)) {
//            $locale = $orderedLocales[0];
//        }
        // Note: GLXY-1422：多语言搜索或者模糊搜索的时候，应该把所有能match搜索条件的语言都找出来，而不是只找用户当前的选择的语言。
        $translations_keys = array_pluck($conditions, '0');
        foreach ($conditions as $key =>  $where) {
            $where[0] = "t$key.value";
            $query->whereExists(function($q) use ($translations_keys, $key, $model, $where){
                $q->selectRaw('1')->from("translations as t$key")
                    ->whereRaw("id = t$key.related_id")
                    ->whereIn("t$key.key", $translations_keys)
                    ->where([
                        ["t$key.namespace", '=', $model]
                    ]);
                $q = $this->_applyCondition($q, $where);
            });
        }
        _cz_log($query->toSql(), $query->getBindings());
        return $query;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder $query
     * @param array $where
     * @return mixed
     */
    private function _applyCondition($query, array $where)
    {
        if (count($where) === 2) {
            if ($where[0] === 'raw_sql') {
                $query = $query->whereRaw($where[1]);
            } else {
                $query = $query->where([$where[0] => $where[1]]);
            }
        }
        // raw_sql query may have multiple commas
        elseif (count($where) > 2 && $where[0] === 'raw_sql') {
            unset($where[0]);
            $where = array_values($where);
            $query = $query->whereRaw(implode(',', $where));
        }

        elseif (count($where) === 3) {
            switch (strtolower($where[1])) {
                case "=" :
                case "<" :
                case ">" :
                case ">=" :
                case "<=" :
                case "<>" :
                    $query->where([
                        [$where[0], $where[1], $where[2]]
                    ]);
                    break;
                case "in" :
                    $query = $query->whereIn($where[0], explode('~', $where[2]));
                    break;
                case "not" :
                    $query = $query->whereNotIn($where[0], explode('~', $where[2]));
                    break;
                case "like" :
                    $query->where([
                        [$where[0], "like", "%" . $where[2] . "%"]
                    ]);
                    break;
                default :
                    break;
            }
        }

        return $query;
    }
}