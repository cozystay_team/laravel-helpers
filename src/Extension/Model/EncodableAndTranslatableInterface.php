<?php
/**
 * Created by PhpStorm.
 * User: terry
 * Date: 2017-10-07
 * Time: 10:40 AM
 */

namespace Galaxy\Helpers\Extension\Model;


interface EncodableAndTranslatableInterface
{

    public function processed(): bool;

    public function setProcessed(bool $processed);

    public function loadFields();

    public function beforeSave();
}