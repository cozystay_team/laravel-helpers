<?php

namespace Galaxy\Helpers\Extension\Provider;

/**
 * Created by PhpStorm.
 * User: qiyuzhao
 * Date: 2017-09-08
 * Time: 11:27 AM
 */


use Galaxy\Helpers\Extension\Model\EncodableAndTranslatableInterface;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Support\ServiceProvider;

class EncodeAndTranslationProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        /**
         * $ourModelInstanceClass is a string of Model Instance Class name
         * $more is an array of real Model Object
         */
        $events->listen('eloquent.loaded: *', function ($ourModelInstanceClass, $more) {
            // "*" is a wildcard matching all models,
            // replace if you need to apply to only one model,
            // for example "App\Article"

            //translation code goes here
            $modelInstance = $more[0];
            if ($modelInstance instanceof EncodableAndTranslatableInterface) {
                $modelInstance->loadFields();  //translate, merge locale into Model
            }

        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public
    function register()
    {
        //
    }
}