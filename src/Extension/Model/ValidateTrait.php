<?php
/**
 * Created by PhpStorm.
 * User: terrywang
 * Date: 2017/5/10
 * Time: 17:32
 */

namespace Galaxy\Helpers\Extension\Model;

use Galaxy\Helpers\Exception\ExceptionMapping;
use Illuminate\Support\Facades\Validator;

trait ValidateTrait
{
    /**
     * @param array $data
     * @param array $except
     */
    public function validate(array $data, array $except = [])
    {
        if (method_exists($this, "getRules")) {
            $rules = $this->getRules($data, $this);
        } else {
            $rules = $this->rules;
        }

        if (!empty($except)) {
            $rules = array_except($rules, $except);
        }

        $v = Validator::make($data, $rules);

        if ($v->fails()) {

            throw ExceptionMapping::exceptionFromErrorCode(40001, $v->errors()->getMessages());

        };
    }


}