<?php
/**
 * Created by PhpStorm.
 * User: mm_cz
 * Date: 2018-08-30
 * Time: 6:00 PM
 */

namespace Galaxy\Helpers;

class SimpleTimer
{
    public $timer_name;
    private $_timer = ['_start'=>0, '_lastCheck'=>0];

    public function __construct($name=null)
    {
        $this->reset($name);
    }

    public function reset($name=null){
        $this->timer_name = $name??$this->timer_name;
        $this->_timer['_start'] = microtime(true);
        $this->_timer['_lastCheck'] = $this->_timer['_start'];

        return $this;
    }
    public function record($name){
        $this->_timer[$name] = microtime(true) - $this->_timer['_lastCheck'];
        $this->_timer['_lastCheck'] = microtime(true);
        $this->_timer['total'] = $this->_timer['_lastCheck'] - $this->_timer['_start'];

        return $this;
    }
    public function stop(){
        $this->_timer['_lastCheck'] = microtime(true);
        $this->_timer['_stop'] = $this->_timer['_lastCheck'];
        $this->_timer['total'] = $this->_timer['_lastCheck'] - $this->_timer['_start'];

        return $this;
    }
    public function flushAllToLog($name=null){
        $name = $name??$this->timer_name;
        $timer = array_map(function($val){ return round($val, 5); }, $this->_timer);

        _cz_logger_debug("==== {$name}: {$timer['total']}====");
        foreach ($timer as $key => $time) {
            _cz_logger_debug("| SimpleTimer", $key, $time);
        }

        return true;
    }
}